import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.TimeZone;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.DatabaseMetaData;
import com.mysql.jdbc.Statement;


public class MS3 {

	private static final char SEPARATOR = ',';
    private static final char QUOTE = '"';
    
 public static void main(String[] args) throws Exception
 {
    Statement stmt = null;
    String importFile = "src//ms3Interview.csv";
    int i = 0;
    
    String databaseURL = "jdbc:mysql://localhost:3306/connect";
    String user = "root";
    String password = "Kingut123";
    Connection conn = null;
    try {
        Class.forName("com.mysql.jdbc.Driver");
        conn = (Connection) DriverManager.getConnection(databaseURL,user,password);
        if (conn != null) {
            System.out.println("Connected to the database");
        }
        stmt = (Statement) conn.createStatement();
        
        String sql = "CREATE TABLE X " +
                     "(A VARCHAR(255), " +
                     " B VARCHAR(255), " + 
                     " C VARCHAR(255), " + 
                     " D VARCHAR(255), " + 
                     " E VARCHAR(255), " + 
                     " F VARCHAR(255), " + 
                     " G VARCHAR(255), " +
                     " H VARCHAR(255), " + 
                     " I VARCHAR(255), " + 
                     " J VARCHAR(255))";    //DDL COMMAND - CREATE (create table X)

        stmt.executeUpdate(sql);
    
        // OTHER DDL COMMMANDS :  
        
        		/*ALTER and CHANGE (ALTER will change the structure of the database and CHANGE is used to make changes)
        		Query:-
        		stmt.executeUpdate("ALTER TABLE X CHANGE A O VARCHAR(255);");*/
        		
               	/*DROP TABLE (It will delete table X)
                Query:-
                stmt.executeUpdate("DROP TABLE X");*/
        
        		/*DROP TABLE (It will delete all the data from table X, leaving only table structure behind)
                Query:-
                stmt.executeUpdate("TRUNCATE TABLE X");*/
         
        
        
        System.out.println("table created");
    } catch (ClassNotFoundException ex) {
        System.out.println("Could not find database driver class");
        ex.printStackTrace();
    } 
    catch (SQLException ex) {
        System.out.println("An error occurred. Maybe user/password is invalid");
        ex.printStackTrace();
    
        stmt = (Statement) conn.createStatement();
        
        String sql;
        ResultSet set;
        Boolean flag = true, flag1 = true;
        int gooddata = 0, baddata = 0, total = 0;
        DatabaseMetaData m = (DatabaseMetaData) conn.getMetaData();
        Scanner scanner = new Scanner(new File(importFile));
        ArrayList<String> list = parseLine(scanner.nextLine()); 
        
        set = m.getTables(null, null, "%", null);
        
        while(set.next())
        { 
            if (set.getString(3).equals("SUCCESS"))
            {
                flag = false;  
            }
            if(set.getString(3).equals("FAIL"))
            {
                flag1 = false;
            }
        }    
        try{
            if(flag)// Create Table SUCCESS for gooddata
            {
                sql = "CREATE TABLE SUCCESS " + "(A TEXT," + "B TEXT," + "C TEXT," +
                         "D TEXT," + "E TEXT," + "F TEXT," + "G TEXT," +
                         "H TEXT," + "I TEXT," + "J TEXT)" ;
                
                stmt.executeUpdate(sql);  
            }        

            if (flag1)// Create Table FAIL for baddata
            {
                sql = "CREATE TABLE FAIL" + "(A TEXT," + "B TEXT," + "C TEXT," +
                         "D TEXT," + "E TEXT," + "F TEXT," + "G TEXT," +
                         "H TEXT," + "I TEXT," + "J TEXT)" ;
                
                stmt.executeUpdate(sql);
            } 
        }catch(Exception e){
        	e.printStackTrace();
        }
                System.out.println("Tables Created Succesfully");
        
            stmt.close();
        
        while (scanner.hasNext() && i < 2000) 
        {
        	list = parseLine(scanner.nextLine());
            stmt = (Statement) conn.createStatement();
           try{
          // Check if record is complete and send to MS3 table in data base  
          if(((ArrayList<String>) list).get(0).equals("false"))       
          {
        	  System.out.println(list+"\n");
        	  try{
            sql = "INSERT INTO SUCCESS (A,B,C,D,E,F,G,H,I,J) "+
                  "VALUES ('"+list.get(1)+"','" +list.get(2)+ "','" + list.get(3)+ "','" + list.get(4)+ "','" + list.get(5)+ "','" + list.get(6)+ "','" + list.get(7)+ "','" + list.get(8)+ "','" + list.get(9)+ "','" + list.get(10)+ "');"; 
            
            stmt.executeUpdate(sql);
            
        	  }catch(IndexOutOfBoundsException e){
        		  baddata++;
                  total++;
        	  }
            gooddata++;
            total++;
            
            }else 
            {
            writer("\""+list.get(1)+"," +list.get(2)+ "," + list.get(3)+ "," + list.get(4)+ "," + list.get(5)+ "," + list.get(6)+ "," + list.get(7)+ "," + list.get(8)+ "," + list.get(9)+ "," + list.get(10)+ ";\""); 
            baddata++;       
            total++;
            }
            
           }catch(Exception e){
        	   e.printStackTrace();
           }
           i++;
           System.out.println("\n"+i);
        }
        System.out.println(" Total "+ total + "" + " Good " + gooddata+ "" +" Bad " +baddata);
        
        String log = " # of records received: " + total + "" + " # of records successful: " + gooddata + "" + " # of records failed: " + baddata;
        
        write(log);             //log file
                
        stmt.close();
        conn.close();
        scanner.close();
    }
 }

    public static void writer(String str) 
    {
        PrintWriter pw;
		try {
			pw = new PrintWriter(new FileOutputStream(new File("bad_data_timestamp.csv"), true) );
			pw.append(str);    // writing bad data to csv file
	        pw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		
    }          
    
    
         public static void write(String s) throws IOException {
         writelog("log.txt", s);
         }
    
         public static void writelog(String f, String s) throws IOException {
         TimeZone t = TimeZone.getTimeZone("EST"); 
         Date date; 
         date = new Date(0);
         DateFormat dateformat = new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss.SS");
         dateformat.setTimeZone(t);
         String currentTime = dateformat.format(date);
        
         FileWriter fw = new FileWriter(f, true);
         
         fw.write(currentTime + s);
         fw.flush();
         fw.close();
     }
 

  
         
         
   /*
  Reference for below code = https://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/
  */
        
         
         
         
         
         
         
         
    public static ArrayList<String> parseLine(String cvsLine) {
        return parseLine(cvsLine, SEPARATOR, QUOTE);
    }

    public static ArrayList<String> parseLine(String cvsLine, char separators) {
        return parseLine(cvsLine, separators, QUOTE);
    }

    @SuppressWarnings({ "rawtypes", "unchecked", "null" })
	public static ArrayList<String> parseLine(String cvsLine, char separators, char customQuote) {

		ArrayList<String> result = new ArrayList<String>();

        if (cvsLine == null && cvsLine.isEmpty()) {
            return result;
        }

        if (customQuote == ' ') {
            customQuote = QUOTE;
        }

        if (separators == ' ') {
            separators = SEPARATOR;
        }

        StringBuffer curVal = new StringBuffer();
        boolean inQuotes = false;
        boolean startCollectChar = false;
        boolean doubleQuotesInColumn = false;
        
        char[] chars = cvsLine.toCharArray();
        
        result.add("false");

        for (char ch : chars) {

            if (inQuotes) {
                startCollectChar = true;
                if (ch == customQuote) {
                    inQuotes = false;
                    doubleQuotesInColumn = false;
                } else {

                    //Fixed : allow "" in custom quote enclosed
                    if (ch == '\"') {
                        if (!doubleQuotesInColumn) {
                            curVal.append(ch);
                            doubleQuotesInColumn = true;
                        }
                    } else {
                        curVal.append(ch);
                    }

                }
            } else {
                if (ch == customQuote) {

                    inQuotes = true;

                    //Fixed : allow "" in empty quote enclosed
                    if (chars[0] != '"' && customQuote == '\"') {
                        curVal.append('"');
                    }

                    //double quotes in column will hit this!
                    if (startCollectChar) {
                        curVal.append('"');
                    }

                } else if (ch == separators) {
                       
                                                                //Check for a missing element and set bad data flag to true
                    if(curVal.toString().length() <= 3){
                    ((java.util.List) result).set(0 , "true");
                    }
                    
                    result.add(curVal.toString());

                    curVal = new StringBuffer();
                    startCollectChar = false;

                } else if (ch == '\r') {
                    //ignore LF characters
                    continue;
                } else if (ch == '\n') {
                    //the end, break!
                    break;
                } else {
                    curVal.append(ch);
                }
            }

        }
        
        result.add(curVal.toString());

        return result;
    }


}
